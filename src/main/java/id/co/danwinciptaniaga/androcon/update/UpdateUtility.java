package id.co.danwinciptaniaga.androcon.update;

import java.io.File;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import androidx.core.content.FileProvider;
import androidx.preference.PreferenceManager;
import id.co.danwinciptaniaga.androcon.AndroconConfig;

public class UpdateUtility {
  private static final String TAG = UpdateUtility.class.getSimpleName();

  // jeda query status download
  public static final int DOWNLOAD_PROGRESS_DELAY = 1000;

  public static final String ACTION_DOWNLOAD_COMPLETE = "id.co.danwinciptaniaga.androcon.action.DOWNLOAD_COMPLETE";
  public static final String EXTRA_DOWNLOAD_STATUS = "extra_download_status";
  public static final String DOWNLOAD_URI = "download_Uri";

  public static final int INSTALLER_STATUS_NONE = -1;
  public static final int INSTALLER_STATUS_READY = 100;
  public static final int INSTALLER_STATUS_FAILED = 200;

  public static final String SP_DOWNLOAD_ID = "downloadId";
  public static final String SP_CURRENT_VERSION_MARKER = "currentVersionMarker";

  public static final String URL_REST_UTILITY_DOWNLOAD_VERSION = "rest/utility/downloadVersion";
  public static final String PATH_INSTALLER = "installer";

  public static void setDownloadId(Context context, long downloadId) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    sp.edit().putLong(SP_DOWNLOAD_ID, downloadId).commit();
  }

  public static long getDownloadId(Context context) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    return sp.getLong(SP_DOWNLOAD_ID, -1);
  }

  public static void clearDownloadId(Context context) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    sp.edit().remove(SP_DOWNLOAD_ID).commit();
  }

  public static void setInstallerStatus(Context context, int installerStatus) {
    HyperLog.d(TAG, "setInstallerStatus: " + installerStatus);
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    sp.edit().putInt("installerStatus", installerStatus).commit();
  }

  public static File getUpdateFilePath(Context context, String apkFileName) {
    File updateFile = new File(
        context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
            + File.separator + PATH_INSTALLER, apkFileName);
    return updateFile;
  }

  public static int getInstallerStatus(Context context) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    int result = sp.getInt("installerStatus", INSTALLER_STATUS_NONE);
    HyperLog.d(TAG, "getInstallerReady: " + result);
    return result;
  }

  public static void triggerUpdate(Context context, String apkFileName) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      File downloadTargetDir = new File(
          context.getApplicationContext().getExternalFilesDir(
              Environment.DIRECTORY_DOWNLOADS), UpdateUtility.PATH_INSTALLER);
      File updateFile = new File(downloadTargetDir, apkFileName);
      Uri uri = FileProvider.getUriForFile(context,
          context.getApplicationContext().getPackageName() + ".provider", updateFile);
      Intent install = new Intent(Intent.ACTION_VIEW);
      install.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
      install.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
      install.setData(uri);
      context.startActivity(install);
    } else {
      File downloadTargetDir = new File(
          context.getApplicationContext().getExternalFilesDir(
              Environment.DIRECTORY_DOWNLOADS), UpdateUtility.PATH_INSTALLER);
      File updateFile = new File(downloadTargetDir, apkFileName);
      Uri uri = Uri.fromFile(updateFile);
      Intent install = new Intent(Intent.ACTION_VIEW);
      install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      install.setDataAndType(uri, "application/vnd.android.package-archive");
      context.startActivity(install);
    }
  }

  public static Uri getDownloadUpdateUri(AndroconConfig androconConfig) {
    return Uri.parse(androconConfig.getAndroconUrl() + URL_REST_UTILITY_DOWNLOAD_VERSION);
  }

  public static String generateVersionMarker(PackageInfo pi) {
    long versionCode;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
      versionCode = pi.getLongVersionCode();
    } else {
      versionCode = pi.versionCode;
    }
    long updateTime = pi.lastUpdateTime;
    String marker = String.format("%s|%s", versionCode, updateTime);
    return marker;
  }

  public static String getVersionMarker(Context context) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    String savedMarker = sp.getString(SP_CURRENT_VERSION_MARKER, null);
    return savedMarker;
  }

  public static void setVersionMarker(Context context, String versionMarker) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
    sp.edit().putString(SP_CURRENT_VERSION_MARKER, versionMarker).commit();
  }
}
