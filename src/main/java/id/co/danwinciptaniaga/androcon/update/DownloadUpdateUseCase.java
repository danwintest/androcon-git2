package id.co.danwinciptaniaga.androcon.update;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.google.common.net.HttpHeaders;
import com.google.common.util.concurrent.AsyncCallable;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.R;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AndroconUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.DownloadHelper;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public class DownloadUpdateUseCase extends
    BaseObservable<DownloadUpdateUseCase.Listener, DownloadUpdateUseCase.DownloadUpdateResult> {
  private static final String TAG = DownloadUpdateUseCase.class.getSimpleName();

  public interface Listener {

    void onDownloadUpdateStarted(Resource<DownloadUpdateResult> loading);

    void onDownloadUpdateSuccess(Resource<DownloadUpdateResult> response);

    void onDownloadUpdateFailure(Resource<DownloadUpdateResult> response);

  }

  public static class DownloadUpdateResult {

    private Integer status;
    private Integer progress;

    public DownloadUpdateResult(Integer status, Integer progress) {
      this.status = status;
      this.progress = progress;
    }

    public Integer getStatus() {
      return status;
    }

    public Integer getProgress() {
      return progress;
    }

  }

  private final AppExecutors appExecutors;
  private final ListeningScheduledExecutorService scheduledBgExecutor;
  private final AndroconConfig androconConfig;
  private final LoginUtil loginUtil;
  private final DownloadManager dm;
  private final Object MONITOR = new Object();
  private boolean isMonitoring = false;

  private final AsyncCallable monitorDownload = new AsyncCallable() {
    @Override
    public ListenableFuture call() {
      return scheduledBgExecutor.submit(new Runnable() {
        @Override
        public void run() {
          long downloadId = UpdateUtility.getDownloadId(application);
          HyperLog.d(TAG, "Periodic Monitoring download Id: " + downloadId);
          Cursor cursor = DownloadHelper.queryDownload(dm, downloadId);
          Integer status = null;
          try {
            if (cursor.moveToFirst()) {
              status = DownloadHelper.getDownloadStatus(cursor);
              long currentBytes = DownloadHelper.getBytesDownloaded(cursor);
              long totalBytes = DownloadHelper.getTotalSizeBytes(cursor);
              HyperLog.d(TAG,
                  String.format("Status=%s, Download bytes: %s/%s", status, currentBytes,
                      totalBytes));
              Integer percent = 0;
              if (totalBytes > 0) {
                percent = Math.min(Math.round(currentBytes * 100L / totalBytes), 100);
              }
              switch (status) {
              case DownloadManager.STATUS_PENDING:
                notifySuccess(application.getString(R.string.msg_download_update_pending),
                    new DownloadUpdateResult(status, 0));
                internalScheduleMonitor();
                break;
              case DownloadManager.STATUS_RUNNING:
                HyperLog.d(TAG, "Set progress: " + percent);
                notifyStart(application.getString(R.string.msg_download_update_progress, percent),
                    new DownloadUpdateResult(status, percent));
                internalScheduleMonitor();
                break;
              case DownloadManager.STATUS_SUCCESSFUL:
                notifySuccess(application.getString(R.string.msg_download_update_success),
                    new DownloadUpdateResult(status, 100));
                setIsMonitoring(false);
                break;
              case DownloadManager.STATUS_PAUSED:
                String pausedReason = DownloadHelper.getReasonString(application, cursor);
                notifySuccess(application.getString(R.string.msg_download_update_paused,
                    pausedReason),
                    new DownloadUpdateResult(status, percent));
                internalScheduleMonitor();
                break;
              case DownloadManager.STATUS_FAILED:
                String failedReason = DownloadHelper.getReasonString(application, cursor);
                notifyFailure(
                    application.getString(R.string.msg_download_update_failed_reason, failedReason),
                    null, null);
                setIsMonitoring(false);
                break;
              }
            } else {
              setIsMonitoring(false);
            }
          } catch (Exception e) {
            HyperLog.e(TAG, "Problem!!!", e);
            setIsMonitoring(false);
          } finally {
            cursor.close();
          }
        }
      });
    }
  };

  @Inject
  public DownloadUpdateUseCase(Application application, AndroconConfig androconConfig,
      AppExecutors appExecutors, LoginUtil loginUtil) {
    super(application);
    dm = (DownloadManager) application.getSystemService(Context.DOWNLOAD_SERVICE);
    this.androconConfig = androconConfig;
    this.appExecutors = appExecutors;
    this.scheduledBgExecutor = MoreExecutors.listeningDecorator(
        Executors.newScheduledThreadPool(1));
    this.loginUtil = loginUtil;
  }

  public long downloadUpdate() {
    ListenableFuture<Long> downloadLf = Futures.submit(new Callable<Long>() {
      @Override
      public Long call() {
        Uri uri = UpdateUtility.getDownloadUpdateUri(androconConfig);
        HyperLog.d(TAG, "download_update clicked");

        // downloadTargetDir = /Android/data/id.co.danwinciptaniaga.npmdsandroid/files/Download/installer
        File downloadTargetDir = new File(
            application.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            UpdateUtility.PATH_INSTALLER);
        if (!downloadTargetDir.exists()) {
          boolean mkDirResult = downloadTargetDir.mkdir();
          HyperLog.i(TAG, "Creating " + downloadTargetDir.getAbsolutePath() + ": " + mkDirResult);
        } else {
          HyperLog.i(TAG, downloadTargetDir.getAbsolutePath() + " exists");
        }
        File updateFile = new File(downloadTargetDir, androconConfig.getApkFileName());
        if (updateFile.exists()) {
          boolean deleteResult = updateFile.delete();
          HyperLog.i(TAG, "Delete " + updateFile.getAbsolutePath() + ": " + deleteResult);
        }
        Uri updateFileUri = Uri.fromFile(updateFile);
        HyperLog.i(TAG, "Download will be saved at Uri: " + updateFileUri);

        DownloadManager.Request request = new DownloadManager.Request(uri)
            .setTitle("Download versi baru NPMDS")
            .addRequestHeader(AndroconUtility.HTTP_HEADER_ADID, androconConfig.getAppDeviceId())
            .addRequestHeader(HttpHeaders.AUTHORIZATION, "Bearer " + loginUtil.getSavedToken())
            .setMimeType("application/vnd.android.package-archive")
            .setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            .setDestinationUri(updateFileUri);
        long downloadId = dm.enqueue(request);
        UpdateUtility.setDownloadId(application, downloadId);

        return downloadId;
      }
    }, appExecutors.backgroundIO());
    long result = 0;
    try {
      result = downloadLf.get();
      HyperLog.i(TAG, "Initiated download: " + result);
      scheduleMonitor();
    } catch (Exception e) {
      HyperLog.e(TAG, "Problem initiating download", e);
      notifyFailure("Gagal mendownload versi baru", null, e);
    }
    return result;
  }

  public void scheduleMonitor() {
    synchronized (MONITOR) {
      HyperLog.d(TAG,
          "scheduleMonitor: listeners=" + getListeners().size() + ", isMonitoring=" + isMonitoring);
      if (!isMonitoring()) {
        internalScheduleMonitor();
      } else {
        setIsMonitoring(false);
      }
    }
  }

  private void internalScheduleMonitor() {
    synchronized (MONITOR) {
      if (getListeners().size() > 0) {
        // jadwalkan kalau ada listener
        if (!isMonitoring())
          setIsMonitoring(true);
        Futures.scheduleAsync(monitorDownload, UpdateUtility.DOWNLOAD_PROGRESS_DELAY,
            TimeUnit.MILLISECONDS, (ScheduledExecutorService) scheduledBgExecutor);
      } else {
        // kalau tidak ada listener, berhenti
        setIsMonitoring(false);
      }
    }
  }

  public void setIsMonitoring(boolean isMonitoring) {
    synchronized (MONITOR) {
      this.isMonitoring = isMonitoring;
    }
  }

  public boolean isMonitoring() {
    synchronized (MONITOR) {
      return this.isMonitoring;
    }
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<DownloadUpdateResult> result) {
    listener.onDownloadUpdateStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<DownloadUpdateResult> result) {
    listener.onDownloadUpdateSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<DownloadUpdateResult> result) {
    listener.onDownloadUpdateFailure(result);
  }
}
