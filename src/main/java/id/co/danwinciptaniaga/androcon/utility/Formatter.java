package id.co.danwinciptaniaga.androcon.utility;

import java.text.SimpleDateFormat;

public class Formatter {
  public static SimpleDateFormat SDF_ISO_8601 = new SimpleDateFormat(
      "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
  public static final SimpleDateFormat SDF_yyyyMMdd_HHmmss = new SimpleDateFormat(
      "yyyyMMdd_HHmmss");
}
