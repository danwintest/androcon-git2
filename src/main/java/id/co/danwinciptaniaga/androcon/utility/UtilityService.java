package id.co.danwinciptaniaga.androcon.utility;

import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.acs.data.LatestAppVersionResponse;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UtilityService {
  @GET("rest/utility/latestVersion")
  ListenableFuture<LatestAppVersionResponse> getLatestVersion();

  @GET("rest/v2/files/{id}")
  ListenableFuture<ResponseBody> getFile(@Path("id") UUID fileId);

  @Multipart
  @POST("rest/s/logfile")
  ListenableFuture<ResponseBody> uploadLogFile(@Part MultipartBody.Part file);
}
