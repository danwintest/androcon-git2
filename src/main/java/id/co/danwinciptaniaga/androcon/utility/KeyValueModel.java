package id.co.danwinciptaniaga.androcon.utility;

import java.util.Objects;

public class KeyValueModel<K, V> {
  private K key;
  private V value;

  public KeyValueModel(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public void setKey(K key) {
    this.key = key;
  }

  public V getValue() {
    return value;
  }

  public void setValue(V value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object obj) {
    boolean result = false;
    if (obj != null && this.getClass().isAssignableFrom(obj.getClass())) {
      result = getKey() != null && Objects.equals(getKey(), ((KeyValueModel) obj).getKey());
    }
    return result;
  }

  @Override
  public String toString() {
    return value != null ? value.toString() : null;
  }
}