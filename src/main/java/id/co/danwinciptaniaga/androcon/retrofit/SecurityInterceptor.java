package id.co.danwinciptaniaga.androcon.retrofit;

import java.io.IOException;

import org.jetbrains.annotations.NotNull;

import android.accounts.AccountManager;
import android.app.Application;
import android.util.Base64;
import android.util.Log;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.HasAccountType;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class SecurityInterceptor implements Interceptor {
  private static final String TAG = SecurityInterceptor.class.getSimpleName();
  private final Application application;
  private final AndroconConfig androconConfig;
  private final LoginUtil loginUtil;
  private String authorization;

  public SecurityInterceptor(Application application, AndroconConfig androconConfig,
      LoginUtil loginUtil) {
    this.application = application;
    this.loginUtil = loginUtil;
    this.androconConfig = androconConfig;
  }

  @NotNull
  @Override
  public Response intercept(@NotNull Chain chain) throws IOException {
    Request nextRequest = chain.request();
    String token = loginUtil.getSavedToken();

    String originalAuth = nextRequest.header("Authorization");
    // kalau Retrofit sudah menyebutkan Authorization header, maka tidak perlu handle lagi di sini
    if (originalAuth == null) {
      // belum ada Authorization header
      if (token == null) {
        // kalau belum ada token, hanya bisa mengirimkan Basic Authentication header
        String s =
            androconConfig.getAndroconClientId() + ":" + androconConfig.getAndroconClientSecret();
        authorization = "Basic " + Base64.encodeToString(s.getBytes(), Base64.NO_WRAP);
      } else {
        // kalau sudah ada token
        authorization = "Bearer " + token;
      }
      Request.Builder builder = nextRequest.newBuilder()
          .addHeader("Authorization", authorization);
      // nextRequest = yang sudah dimodifikasi
      nextRequest = builder.build();
    } else {
      // sudah ada Authorization header
      // nextRequest = original Request
    }

    Response response = chain.proceed(nextRequest);
    if (HasAccountType.class.isAssignableFrom(application.getClass())) {
      HasAccountType hat = (HasAccountType) application;
      if (invalidToken(response)) {
        AccountManager am = AccountManager.get(application);
        am.invalidateAuthToken(hat.getAccountType(), token);
        loginUtil.clearToken();
      }
    } else {
      Log.w(TAG, "Unable to handle invalidToken globally");
    }
    return response;
  }

  private boolean invalidToken(Response response) {
    boolean result = false;
    if (response.code() == 401) {
      result = true;
    }
    return result;
  }
}
