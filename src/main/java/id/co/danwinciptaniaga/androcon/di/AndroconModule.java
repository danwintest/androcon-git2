package id.co.danwinciptaniaga.androcon.di;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.UserService;
import id.co.danwinciptaniaga.androcon.registration.DeviceRegistrationService;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import okhttp3.OkHttpClient;

@Module
@InstallIn(ApplicationComponent.class)
public class AndroconModule {

  @Provides
  @Singleton
  public OkHttpClient.Builder providerOkHttpClientBuilder(AndroconConfig androconConfig) {
    return new OkHttpClient.Builder()
        .connectTimeout(androconConfig.getRetrofitHttpConnectTimeout(), TimeUnit.SECONDS)
        .readTimeout(androconConfig.getRetrofitHttpReadTimeout(), TimeUnit.SECONDS);
  }

  @Provides
  public AuthService provideAuthService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(AuthService.class);
  }

  @Provides
  public UserService provideUserService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(UserService.class);
  }

  @Provides
  public DeviceRegistrationService provideDeviceRegistrationService(
      WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(DeviceRegistrationService.class);
  }

  @Provides
  public UtilityService providerUtilityService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(UtilityService.class);
  }
}
