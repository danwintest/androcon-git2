package id.co.danwinciptaniaga.androcon.registration;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.acs.data.DeviceRegistrationResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface DeviceRegistrationService {

  @FormUrlEncoded
  @POST("rest/device/register")
  Call<DeviceRegistrationResponse> registerDevice(@Header("Authorization") String authorization,
      @Field("product") String product,
      @Field("phoneModel") String phoneModel,
      @Field("androidVersion") String androidVersion,
      @Field("isRootDetected") Boolean isRootDetected,
      @Field("pnToken") String fcmToken);
}

