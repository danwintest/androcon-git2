package id.co.danwinciptaniaga.androcon.registration;

import java.io.IOException;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public class RegisterDeviceUseCase
    extends BaseObservable<RegisterDeviceUseCase.Listener, Boolean> {

  private static final String TAG = RegisterDeviceUseCase.class.getSimpleName();

  private final AppExecutors appExecutors;
  private final RegisterDeviceUseCaseSync registerDeviceUseCaseSync;

  public interface Listener {
    void onRegisterDeviceStarted(Resource<Boolean> result);

    void onRegisterDeviceSuccess(Resource<Boolean> result);

    void onRegisterDeviceFailed(Resource<Boolean> result);
  }

  @Inject
  public RegisterDeviceUseCase(Application application, AppExecutors appExecutors,
      RegisterDeviceUseCaseSync registerDeviceUseCaseSync) {
    super(application);
    this.appExecutors = appExecutors;
    this.registerDeviceUseCaseSync = registerDeviceUseCaseSync;
  }

  public void registerDevice() {
    appExecutors.networkIO().execute(new Runnable() {
      @Override
      public void run() {
        notifyStart("Mulai registrasi perangkat", null);
        boolean result = false;
        try {
          result = registerDeviceUseCaseSync.registerToServer();
          if (result) {
            notifySuccess("Registrasi perangkat berhasil", result);
          } else {
            notifyFailure("Gagal melakukan registrasi perangkat", null, null);
          }
        } catch (IOException e) {
          HyperLog.w(TAG, "Problem registering device to server ASYNC", e);
          notifyFailure("Gagal menghubungi server, harap coba kembali setelah beberapa saat", null,
              e);

        }
      }
    });

  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<Boolean> result) {
    listener.onRegisterDeviceStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<Boolean> result) {
    listener.onRegisterDeviceSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<Boolean> result) {
    listener.onRegisterDeviceFailed(result);
  }
}
