package id.co.danwinciptaniaga.androcon.glide;

import java.io.InputStream;
import java.util.UUID;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;

public class AndroconModelLoader implements ModelLoader<UUID, InputStream> {
  private UtilityService utilityService;

  public AndroconModelLoader(UtilityService utilityService) {
    this.utilityService = utilityService;
  }

  @Nullable
  @Override
  public LoadData<InputStream> buildLoadData(@NonNull UUID uuid, int width, int height,
      @NonNull Options options) {
    return new LoadData<>(new ObjectKey(uuid), new AndroconFetcher(uuid, utilityService));
  }

  @Override
  public boolean handles(@NonNull UUID uuid) {
    return true;
  }
}
