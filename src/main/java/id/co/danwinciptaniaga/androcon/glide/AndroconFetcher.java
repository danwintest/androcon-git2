package id.co.danwinciptaniaga.androcon.glide;

import java.io.InputStream;
import java.util.UUID;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.util.LogTime;
import com.google.common.util.concurrent.ListenableFuture;

import android.util.Log;
import androidx.annotation.NonNull;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import okhttp3.ResponseBody;

public class AndroconFetcher implements DataFetcher<InputStream> {
  private static final String TAG = AndroconFetcher.class.getSimpleName();

  private final UUID fileId;
  private final UtilityService utilityService;
  private ListenableFuture<ResponseBody> downloadLf;

  public AndroconFetcher(UUID fileId, UtilityService utilityService) {
    this.fileId = fileId;
    this.utilityService = utilityService;
  }

  @Override
  public void loadData(@NonNull Priority priority,
      @NonNull DataCallback<? super InputStream> callback) {
    final long startTime = LogTime.getLogTime();
    try {
      downloadLf = utilityService.getFile(fileId);
      ResponseBody responseBody = downloadLf.get();
      callback.onDataReady(responseBody.byteStream());
    } catch (Exception e) {
      if (Log.isLoggable(TAG, Log.DEBUG)) {
        Log.d(TAG, "Failed to load data for Androcon UUID", e);
      }
      callback.onLoadFailed(e);
    } finally {
      if (Log.isLoggable(TAG, Log.VERBOSE)) {
        Log.v(TAG, "Finished androcon fetcher fetch in " + LogTime.getElapsedMillis(startTime));
      }
    }
  }

  @Override
  public void cleanup() {

  }

  @Override
  public void cancel() {
    if (downloadLf != null) {
      downloadLf.cancel(true);
    }
  }

  @NonNull
  @Override
  public Class<InputStream> getDataClass() {
    return InputStream.class;
  }

  @NonNull
  @Override
  public DataSource getDataSource() {
    return DataSource.REMOTE;
  }
}
