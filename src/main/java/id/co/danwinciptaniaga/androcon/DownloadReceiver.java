package id.co.danwinciptaniaga.androcon;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.update.UpdateUtility;

@AndroidEntryPoint
public class DownloadReceiver extends BroadcastReceiver {
  private static final String TAG = DownloadReceiver.class.getSimpleName();

  @Inject
  AndroconConfig androconConfig;

  @Override
  public void onReceive(Context context, Intent intent) {
    HyperLog.i(TAG, "onReceive: " + intent);
    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())) {
      HyperLog.d(TAG, "download complete intent");
      long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
      long downloadId = UpdateUtility.getDownloadId(context);
      if (id == downloadId) {
        HyperLog.d(TAG, "our download complete is complete: " + id);
        DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        // download sudah selesai
        DownloadManager.Query query = new DownloadManager.Query().setFilterById(downloadId);
        Cursor cursor = null;
        try {
          cursor = dm.query(query);
          if (cursor.moveToFirst()) {
            Uri uri = dm.getUriForDownloadedFile(downloadId);
            HyperLog.d(TAG, "Downloaded file Uri: " + uri);
            int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            HyperLog.d(TAG, "Download status: " + status);
            if (status == DownloadManager.STATUS_SUCCESSFUL) {
              UpdateUtility.setInstallerStatus(context, UpdateUtility.INSTALLER_STATUS_READY);
            } else {
              UpdateUtility.setInstallerStatus(context, UpdateUtility.INSTALLER_STATUS_FAILED);
            }
            Intent downloadCompleteIntent = new Intent(UpdateUtility.ACTION_DOWNLOAD_COMPLETE);
            downloadCompleteIntent.putExtra(UpdateUtility.EXTRA_DOWNLOAD_STATUS, status);
            if (uri != null) {
              // ternyata URI bisa kosong
              downloadCompleteIntent.putExtra(UpdateUtility.DOWNLOAD_URI, uri.toString());
            }
            boolean broadcastReceived = LocalBroadcastManager
                .getInstance(context.getApplicationContext())
                .sendBroadcast(downloadCompleteIntent);
            HyperLog.i(TAG, "Broadcast " + intent + ": " + broadcastReceived);
            if (!broadcastReceived) {
              if (status == DownloadManager.STATUS_SUCCESSFUL) {
                HyperLog.i(TAG, "Due to no BroadcastListener, update is triggered here");
                UpdateUtility.triggerUpdate(context, androconConfig.getApkFileName());
              } else {
                HyperLog.w(TAG, "No BroadcastListener, but download is unsuccessful");
                // TODO: toast failure message?
              }
            } else {
              HyperLog.i(TAG, "BroadcastListener invoked, no handling here");
            }
          }
        } finally {
          cursor.close();
        }

        Uri downloadedFileUri = dm.getUriForDownloadedFile(id);
        HyperLog.i(TAG, "Download file Uri: " + downloadedFileUri);
      } else {
        HyperLog.d(TAG, "other people's download complete is complete: " + id);
      }
    }
  }
}