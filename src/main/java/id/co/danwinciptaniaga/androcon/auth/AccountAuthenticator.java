package id.co.danwinciptaniaga.androcon.auth;

import com.hypertrack.hyperlog.HyperLog;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;

public abstract class AccountAuthenticator extends AbstractAccountAuthenticator {
  private final String TAG = getClass().getSimpleName();

  public static final String ARG_GENERAL_LOGIN = "generalLogin";
  public static final String ARG_IS_ADDING_NEW_ACCOUNT = "isAddingNewAccount";
  public static final String ARG_IS_CONFIRMING_CREDENTIALS = "isConfirmingCredentials";
  public static final String ARG_PERFORM_APP_LOGIN = "performAppLogin";
  public static final String ARG_AUTH_TOKEN_TYPE = "authTokenType";

  public static final String USERDATA_REFRESH_TOKEN = "refreshToken";
  public static final String USERDATA_TOKEN_ACQUIRED_AT = "acquiredAt";
  public static final String USERDATA_TOKEN_EXPIRES_IN = "tokenExpiresIn";

  protected final Context mContext;
  protected final GetTokenUseCaseSync ucGetTokenSync;
  protected final LoginUtil mLoginUtil;

  public AccountAuthenticator(Context context, GetTokenUseCaseSync ucGetTokenSync,
      LoginUtil loginUtil) {
    super(context);
    mContext = context;
    mLoginUtil = loginUtil;
    this.ucGetTokenSync = ucGetTokenSync;
  }

  @Override
  public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
    Log.i(TAG, "editProperties called");
    return null;
  }

  @Override
  public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
      String authTokenType, String[] requiredFeatures, Bundle options)
      throws NetworkErrorException {
    Bundle reply = new Bundle();

    boolean isTriggeredFromApp = false;
    if (options != null) {
      isTriggeredFromApp = options.getBoolean(ARG_PERFORM_APP_LOGIN, false);
    }
    Log.i(TAG, "addAccount called" + (isTriggeredFromApp ? "" : " not") + " from app");

    Intent intent = getAddAcountIntent(response, accountType, authTokenType, options);
    // mandatory arguments
    if (options != null)
      intent.putExtras(options);
    intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
    intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
    intent.putExtra(AccountAuthenticator.ARG_AUTH_TOKEN_TYPE, authTokenType);

    // return our AccountAuthenticatorActivity
    reply.putParcelable(AccountManager.KEY_INTENT, intent);
    return reply;
  }

  @Override
  public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account,
      Bundle options) throws NetworkErrorException {
    // method ini mendukung mode:
    // 1. lokal (default)w: verifikasi password/ Biometric Signin dilakukan terhadap Account. Kalau verifikasi lokal gagal, maka akan diteruskan ke server.
    // 2. remote: verifikasi password dilakukan di server
    Log.i(TAG, "confirmCredentials called for " + account.name);

    Intent intent = getConfirmCredentialsIntent(response, account, options);
    // argument yang pasti diperlukan oleh AuthenticationActivity
    if (options != null)
      intent.putExtras(options);
    intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
    intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name);
    intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type);
    intent.putExtra(ARG_IS_CONFIRMING_CREDENTIALS, true);

    Bundle reply = new Bundle();
    reply.putParcelable(AccountManager.KEY_INTENT, intent);
    Log.i(TAG, "confirmCredentials done");
    return reply;
  }

  /**
   * Class konkrit harus mengimplementasikan method ini untuk mengembalikan Class yang dapat melakukan konfirmasi credentials.
   *
   * @param response
   * @param account
   * @param options
   * @return
   */
  protected abstract Intent getConfirmCredentialsIntent(AccountAuthenticatorResponse response,
      Account account, Bundle options);

  @Override
  public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account,
      String authTokenType, Bundle options) throws NetworkErrorException {
    Log.i(TAG, "getAuthToken called");
    boolean performAppLogin = options.getBoolean(AccountAuthenticator.ARG_PERFORM_APP_LOGIN);

    // Extract the username and password from the Account Manager, and ask
    // the server for an appropriate AuthToken.
    final AccountManager am = AccountManager.get(mContext);

    String refreshToken = am.getUserData(account, USERDATA_REFRESH_TOKEN);
    String authToken = am.peekAuthToken(account, authTokenType);
    int expiresIn = -1;
    Log.d(TAG, String.format("Peek AuthToken for %s = %s", account.name, authToken));

    // Lets give another try to authenticate the user
    if (TextUtils.isEmpty(authToken)) {
      Log.d(TAG, "Token is empty");
      if (refreshToken != null) {
        Log.d(TAG, "Refresh token is available");
        Resource<TokenResponse> refreshResult = ucGetTokenSync.refreshToken(
            refreshToken);
        if (Status.SUCCESS.equals(refreshResult.getStatus())) {
          Log.d(TAG, "New token obtained by refreshing");
          authToken = refreshResult.getData().getAccessToken();
          expiresIn = refreshResult.getData().getExpiresIn();
        } else {
          Log.w(TAG, "Refresh Token failed");
        }
      } else {
        Log.d(TAG, "Refresh token is not available");
      }

      if (authToken == null) {
        final String password = am.getPassword(account);
        if (password != null) {
          Log.d(TAG, "Password is available, use it to getToken from server");
          Resource<TokenResponse> ar = ucGetTokenSync.getToken(account.name,
              password);
          if (Status.SUCCESS.equals(ar.getStatus())) {
            TokenResponse tr = ar.getData();
            authToken = tr.getAccessToken();
            expiresIn = tr.getExpiresIn();
            Log.d(TAG, "Obtained token from server using stored password");
          } else {
            Log.e(TAG, "Problem getting token: " + ar);
          }
        } else {
          Log.d(TAG, "Password is not available, cannot attempt to getToken from server");
        }
      }
    }

    // If we get an authToken - we return it
    if (null != authToken && !authToken.isEmpty()) {
      Log.d(TAG, "Returning authToken using Intent, and updating Account");
      AccountManagerUtil.setAccountAuthToken(am, account, authTokenType, authToken,
          refreshToken, System.currentTimeMillis(), expiresIn);
      if (performAppLogin) {
        HyperLog.d(TAG, "Perform App Login during getAuthToken");
        mLoginUtil.saveToken(authToken, authTokenType);
      } else {
        HyperLog.d(TAG, "Not performing App Login during getAuthToken");
      }
      final Bundle result = new Bundle();
      result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
      result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
      result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
      return result;
    }

    Log.d(TAG, "getAuthToken must prompt user again to login");
    // If we get here, then we couldn't access the user's password - so we
    // need to re-prompt them for their credentials. We do that by creating
    // an intent to display our AuthenticatorActivity.
    // pemanggilan AuthenticationActivity ini tidak perlu flag isAdding
    final Intent intent = getAuthenticateIntent(response, account, authTokenType, options);
    if (options != null)
      intent.putExtras(options);
    intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
    intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type);
    intent.putExtra(AccountAuthenticator.ARG_AUTH_TOKEN_TYPE, authTokenType);

    // This is for the case multiple accounts are stored on the device
    // and the AccountPicker dialog chooses an account without auth token.
    // We can pass out the account name chosen to the user of write it
    // again in the Login activity intent returned.
    if (null != account) {
      intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name);
    }
    // TODO: set pesan error kalau ada?

    final Bundle bundle = new Bundle();
    bundle.putParcelable(AccountManager.KEY_INTENT, intent);

    return bundle;
  }

  /**
   * Class konkrit harus mengimplementasikan method ini untuk mengembalikan Class yang dapat melakukan addAccount.
   *
   * @param response
   * @param accountType
   * @param authTokenType
   * @return
   */
  protected abstract Intent getAddAcountIntent(AccountAuthenticatorResponse response,
      String accountType, String authTokenType, Bundle options);

  /**
   * Class konkrit harus mengimplementasikan method ini untuk mengembalikan Class yang dapat melakukan authentication.
   *
   * @param response
   * @param account
   * @param authTokenType
   * @return
   */
  protected abstract Intent getAuthenticateIntent(AccountAuthenticatorResponse response,
      Account account, String authTokenType, Bundle options);

  @Override
  public String getAuthTokenLabel(String authTokenType) {
    Log.i(TAG, String.format("authTokenType: %s", authTokenType));
    return authTokenType;
  }

  @Override
  public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account,
      String authTokenType, Bundle options) throws NetworkErrorException {
    Log.i(TAG, "updateCredentials called");
    return null;
  }

  @Override
  public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account,
      String[] features) throws NetworkErrorException {
    Log.i(TAG, "hasFeatures called");
    return null;
  }
}
