package id.co.danwinciptaniaga.androcon.auth;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.common.base.Preconditions;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import androidx.preference.PreferenceManager;
import dagger.hilt.android.qualifiers.ApplicationContext;

@Singleton
public class LoginUtil {
  private static final String TAG = LoginUtil.class.getSimpleName();

  public static final long MIN_TIME_TO_REFRESH = 3 * 60 * 60 * 1000; // 3 hr

  public static final String SP_AUTH_TOKEN = "authToken";
  public static final String SP_AUTH_TOKEN_TYPE = "authTokenType";
  public static final String SP_USERNAME = "username";
  public static final String SP_EXPIRY = "expiry";
  public static final String SP_LOGGED_IN = "loggedIn";
  public static final String SP_BIOMETRIC_USERNAME = "biometricUsername";
  public static final String SP_ENCRYPTION_IV = "encryptionIv";
  public static final String SP_ENCRYPTED_DATA = "encryptedData";

  public static final String SP_LAST_PAUSED_TIME = "lastPausedTime";
  // preference
  public static final String SP_ENABLE_BIOMETRIC = "enable_biometric";

  private Context context;
  private SharedPreferences sp;

  @Inject
  public LoginUtil(@ApplicationContext Context context) {
    this.context = context;
    sp = PreferenceManager.getDefaultSharedPreferences(context);
  }

  /**
   * Mengembalikan Token yang disimpan di Aplikasi. Token akan disimpan di aplikasi kalau User
   * berhasil melalui proses Authentication, seperti:
   * - login pertama kali dengan Account baru
   * - login pertama kali dengan Account yang ada
   * - confirm credentials yang berhasil
   *
   * @return <code>null</code> kalau Token belum di-cache di aplikasi.
   */
  public String getSavedToken() {
    String token = sp.getString(SP_AUTH_TOKEN, null);
    return token;
  }

  public String getSavedTokenType() {
    String tokenType = sp.getString(SP_AUTH_TOKEN_TYPE, null);
    return tokenType;
  }

  /**
   * Token disimpan di aplikasi ketika User menggunakan aplikasinya.
   *
   * @param token
   * @param tokenType
   */
  public void saveToken(String token, String tokenType) {
    Preconditions.checkNotNull(token, "token cannot be null");
    Preconditions.checkNotNull(tokenType, "tokenType cannot be null");
    sp.edit()
        .putString(SP_AUTH_TOKEN, token)
        .putString(SP_AUTH_TOKEN_TYPE, tokenType)
        .commit();
  }

  /**
   * Mengembalikan Username yang tercatat sedang logged-in.
   *
   * @return Username yang loggedin; <code>null</code> kalau tidak ada yang logged-in.
   */
  public String getUsername() {
    String username = sp.getString(SP_USERNAME, null);
    return username;
  }

  public void saveUsername(String username) {
    sp.edit().putString(SP_USERNAME, username).commit();
  }

  /**
   * Token dilupakan kalau User tidak menggunakan aplikasinya.
   */
  public void clearToken() {
    HyperLog.i(TAG, "clearToken");
    sp.edit()
        .remove(SP_AUTH_TOKEN)
        .remove(SP_AUTH_TOKEN_TYPE)
        .commit();
  }

  public void clearUsername() {
    sp.edit().remove(SP_USERNAME).commit();
  }

  /**
   * Catat ketika User berhasil login.
   *
   * @param username
   * @param authToken
   * @param expiresIn
   */
  public void setLoggedIn(String username, String authToken, int expiresIn) {
    long now = System.currentTimeMillis();
    if (username == null)
      throw new IllegalArgumentException("username must not be null");
    if (authToken == null)
      throw new IllegalArgumentException("authToken must not be null");
    sp.edit()
        .putBoolean(SP_LOGGED_IN, true)
        .putString(SP_USERNAME, username)
        .putString(SP_AUTH_TOKEN, authToken)
        .putLong(SP_EXPIRY, now + expiresIn)
        .commit();
  }

  public boolean isLoggedIn() {
    return sp.getBoolean(SP_LOGGED_IN, false);
  }

  public void logout() {
    HyperLog.i(TAG,"logout()");
    sp.edit()
        .remove(SP_LOGGED_IN)
        .remove(SP_USERNAME)
        .remove(SP_AUTH_TOKEN)
        .remove(SP_AUTH_TOKEN_TYPE)
        .remove(SP_EXPIRY)
        .remove(SP_ENABLE_BIOMETRIC)
        .remove(SP_BIOMETRIC_USERNAME)
        .remove(SP_ENCRYPTION_IV)
        .remove(SP_ENCRYPTED_DATA)
        .remove(SP_LAST_PAUSED_TIME)
        .commit();
  }

  public void setLastPausedTime(long lastPausedTime) {
    sp.edit().putLong(SP_LAST_PAUSED_TIME, lastPausedTime).commit();
  }

  public long getLastPausedTime() {
    return sp.getLong(SP_LAST_PAUSED_TIME, -1);
  }

  public void clearLastPausedTime() {
    sp.edit().remove(SP_LAST_PAUSED_TIME).commit();
  }

  public void setBiometricUser(byte[] iv, String username, byte[] encryptedUsername) {
    HyperLog.d(TAG, "setBiomatricUser username[" + username + "]");
    sp.edit()
        .putString(SP_BIOMETRIC_USERNAME, username)
        .putString(SP_ENCRYPTION_IV, Base64.encodeToString(iv, Base64.NO_WRAP))
        .putString(SP_ENCRYPTED_DATA, Base64.encodeToString(encryptedUsername, Base64.NO_WRAP))
        .commit();
  }

  public String getBiometricUsername() {
    HyperLog.d(TAG, "getBiomatricUsername[" + sp.getString(SP_BIOMETRIC_USERNAME, null) + "]");
    return sp.getString(SP_BIOMETRIC_USERNAME, null);
  }

  public byte[] getEncryptedData() {
    byte[] result = null;
    String value = sp.getString(SP_ENCRYPTED_DATA, null);
    if (value != null) {
      result = Base64.decode(value, Base64.NO_WRAP);
    }
    return result;
  }

  public byte[] getEncryptionIv() {
    byte[] result = null;
    String value = sp.getString(SP_ENCRYPTION_IV, null);
    if (value != null) {
      result = Base64.decode(value, Base64.NO_WRAP);
    }
    return result;
  }

  public void clearBiometricUser() {
    HyperLog.i(TAG,"clearBioMatricUser()");
    sp.edit()
        .remove(SP_ENCRYPTION_IV)
        .remove(SP_BIOMETRIC_USERNAME)
        .remove(SP_ENCRYPTED_DATA)
        .commit();
  }

  public boolean isBiometricEnabled() {
    return sp.getBoolean(SP_ENABLE_BIOMETRIC, false);
  }

  public static Account getAccount(Context context, String accountName, String accountType) {
    AccountManager accountManager = AccountManager.get(context);
    Account[] accounts = accountManager.getAccountsByType(accountType);
    for (Account account : accounts) {
      if (account.name.equalsIgnoreCase(accountName)) {
        HyperLog.d(TAG, "getAccout[" + account + "]");
        return account;
      }
    }
    HyperLog.d(TAG, "getAccout[NULL]");
    return null;
  }

  public Account getAccount(String accountName, String accountType) {
    return getAccount(this.context, accountName, accountType);
  }

  /**
   * Dapatkan Account untuk user logged-in.
   *
   * @param accountType
   * @return <code>null</code> kalau tidak ada yang logged-in, atau kalau Account yang sesuai tidak ditemukan.
   */
  public Account getAccount(String accountType) {
    return getAccount(getUsername(), accountType);
  }

  public boolean shouldRefreshToken(long expiresAt, long minTimeToRefresh) {
    boolean result = false;
    if (isLoggedIn()) {
      if (expiresAt > 0) {
        long now = System.currentTimeMillis();
        if ((expiresAt - now) <= minTimeToRefresh) {
          HyperLog.v(TAG,
              String.format("User is logged-in, %s - %s <= %s, should refresh token", expiresAt,
                  now, minTimeToRefresh));
          result = true;
        } else {
          HyperLog.v(TAG,
              String.format("User is logged-in, %s - %s > %s, should not refresh token yet",
                  expiresAt, now, minTimeToRefresh));
        }
      } else {
        HyperLog.v(TAG, "Expiry not set");
      }
    } else {
      HyperLog.v(TAG, "User is not logged-in, no need to refresh token");
    }
    return result;
  }
}
