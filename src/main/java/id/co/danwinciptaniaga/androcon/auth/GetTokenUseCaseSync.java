package id.co.danwinciptaniaga.androcon.auth;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.WorkerThread;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import retrofit2.Call;
import retrofit2.Response;

public class GetTokenUseCaseSync {
  private static final String TAG = GetTokenUseCaseSync.class.getSimpleName();

  public static final String GRANT_TYPE_PASSWORD = "password";
  public static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
  private final AuthorizationHeaderProvider ahp;
  private final AuthService authService;

  @Inject
  public GetTokenUseCaseSync(AuthorizationHeaderProvider ahp, AuthService authService) {
    this.authService = authService;
    this.ahp = ahp;
  }

  @WorkerThread
  public Resource<TokenResponse> getToken(String username, String password) {
    Resource<TokenResponse> result = null;
    Call<TokenResponse> tokenCall = authService.getTokenCall(ahp.clientAuthorizationHeader(),
        username, password, GRANT_TYPE_PASSWORD);
    try {
      Response<TokenResponse> tokenCallResponse = tokenCall.execute();
      if (tokenCallResponse.isSuccessful()) {
        result = Resource.Builder.success("Token obtained from server", tokenCallResponse.body());
      } else {
        ErrorResponse er = RetrofitUtility.parseErrorBody(tokenCallResponse.errorBody());
        result = Resource.Builder.error("Problem getting token from server", er);
      }

    } catch (Exception e) {
      HyperLog.e(TAG, "Failed to getToken from server", e);
      result = Resource.Builder.error("Problem getting token from server", e);
    }

    return result;
  }

  public Resource<TokenResponse> refreshToken(final String refreshToken) {
    ListenableFuture<TokenResponse> tokenResponseLf = authService.refreshToken(
        ahp.clientAuthorizationHeader(), refreshToken, GRANT_TYPE_REFRESH_TOKEN);
    Resource<TokenResponse> result = null;
    try {
      TokenResponse tr = tokenResponseLf.get();
      result = Resource.Builder.success("Refresh token obtained", tr);
    } catch (Exception e) {
      HyperLog.e(TAG, "Problem refreshing token", e);
      result = Resource.Builder.error("Problem refreshing token", e);
    }
    return result;
  }
}
