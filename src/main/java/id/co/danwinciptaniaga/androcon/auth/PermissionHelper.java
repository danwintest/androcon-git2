package id.co.danwinciptaniaga.androcon.auth;

import java.security.Permission;
import java.util.List;

import com.google.common.base.Preconditions;

public class PermissionHelper {
  public static final String PERMISSION_TYPE_SCREEN = "SCREEN";
  public static final String PERMISSION_TYPE_SPECIFIC = "SPECIFIC";
  public static final String PERMISSION_TYPE_ENTITY_OP = "ENTITY_OP";
  public static final String PERMISSION_TYPE_ENTITY_ATTR = "ENTITY_ATTR";
  public static final String PERMISSION_TYPE_UI = "UI";

  public static final String PERMISSION_VALUE_ALLOW = "ALLOW";
  public static final String PERMISSION_VALUE_DENY = "DENY";
  public static final String PERMISSION_VALUE_VIEW = "VIEW";
  public static final String PERMISSION_VALUE_MODIFY = "MODIFY";

  public static final String PERMISSION_VALUE_HIDE = "HIDE";
  public static final String PERMISSION_VALUE_READ_ONLY = "READ_ONLY";
  public static final String PERMISSION_VALUE_SHOW = "SHOW";

  public static boolean hasPermission(List<PermissionInfo> permissions, String type, String target) {
    boolean result = false;
    Preconditions.checkNotNull(type);
    Preconditions.checkNotNull(target);
    if (permissions != null) {
      for (PermissionInfo pi : permissions) {
        if (type.equals(pi.getType()) && target.equals(pi.getTarget())) {
          if (
              (PERMISSION_TYPE_SCREEN.equals(type)
                  || PERMISSION_TYPE_ENTITY_OP.equals(type)
                  || PERMISSION_TYPE_SPECIFIC.equals(type)
              ) && PERMISSION_VALUE_ALLOW.equals(pi.getValue())) {
            return true;
          } else if (PERMISSION_TYPE_ENTITY_ATTR.equals(type) && !PERMISSION_VALUE_DENY.equals(
              pi.getValue())) {
            return true;
          } else if (PERMISSION_TYPE_UI.equals(type) && !PERMISSION_VALUE_HIDE.equals(
              pi.getValue())) {
            return true;
          }
        }
      }
    }
    return result;
  }
}
