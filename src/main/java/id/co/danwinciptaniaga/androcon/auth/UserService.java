package id.co.danwinciptaniaga.androcon.auth;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface UserService {
  @GET("rest/v2/userInfo")
  Call<UserInfo> getUserCall();

  @GET("rest/v2/permissions")
  Call<List<PermissionInfo>> getUserPermissionsCall();
}
